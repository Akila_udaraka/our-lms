<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>
         <title> News & Events </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw nw1">
            <div class="col-sm-12">
                <h1 class="ne1">News & Events</h1>
            </div>
        </div>
        
        <div class="raw nw2">
            <div class="col-sm-8 col-sm-offset-2">
                <h3><b>NSBM Walk</b></h3><br>
                <img src="Photos/walk.JPG" class="img-responsive" alt=""><br>
                <p style="font-size:12pt">The NSBM Walk 2016 is scheduled for Sunday, 7th February 2016 with the theme of "Strength is Unity". The walk will start from Green Path by 3.00 pm and will walk towards CH & FC ground for the Walk carnival. The proceedings of this programme will be donated to the Children Ward of Cancer Hospital, Maharagama</p>
            </div>
            <br>
            <div class="col-sm-8 col-sm-offset-2">
                <h3><b>Tree Plantation Ceremony of NSBM Green University Town</b></h3><br>
                <img src="Photos/plant.jpg" class="img-responsive" alt=""><br>
                <p style="font-size:12pt">The Tree Plantation Ceremony of NSBM Green University Town was held on the January 8, 2016. Dr. E A Weerasinghe, Chief Executive Officer-NSBM, Board of Directors of NSBM, Dr. Cheryl J. Norton, President of Slippery-rock University, Pennsylvania - USA, Delegates from University College Dublin - Ireland, Delegates from Plymouth University - United Kingdom and the Management of NSBM graced the event</p>
            </div>
            <br>
            <div class="col-sm-8 col-sm-offset-2">
                <h3><b>American Degrees soon at NSBM</b></h3><br>
                <img src="Photos/img002.jpg" class="img-responsive" alt=""><br>
                <p style="font-size:12pt">NSBM the Nation's Premier Degree School will launch degree programmes affiliated to SRU, State University of Pennsylvania, United States very soon.

                    A letter of intent on this regard was signed and exchanged between Dr. E A Weerasinghe-Vice Chancellor of NSBM and Dr. Cheryl J. Norton President of SRU on 8th of January 2016. This will bring the opportunity to Sri Lankan students to follow world-class American degree programmes in the fields of Bio-Science, Chemistry, Physics, Computing and Management at NSBM Green University. </p>
            </div>
            <br>
            <div class="col-sm-8 col-sm-offset-2">
                <h3><b>We Got Talent 2015</b></h3><br>
                <img src="Photos/img001.jpg" class="img-responsive" alt=""><br>
                <p style="font-size:12pt">NSBM We got Talent, the massive talent search organised by the Management of National School of Business Management (NSBM) for the third consecutive year was held in Nelum Pokuna Mahinda Rajapaksha Theatre on 18 December 2015.</p>
            </div>
            <br>
            <div class="col-sm-8 col-sm-offset-2">
                <h3><b>Plymouth University-Graduation Ceremony</b></h3><br>
                <img src="Photos/ply.jpg" class="img-responsive" alt=""><br>
                <p style="font-size:12pt">The inception Graduation Ceremony of the Plymouth University, United Kingdom for the degree programmes offered in collaboration with National School of Business Management (NSBM) was held on 27 November 2015 at BMICH under the distinguished patronage of Hon. Mahinda Samarasinghe - Minister of Skills Development & Vocational Training. 2015.</p>
            </div>
            <br>
            <div class="col-sm-8 col-sm-offset-2">
                <h3><b>Inauguration Ceremony of Degree Programmes</b></h3><br>
                <img src="Photos/img003.jpg" class="img-responsive" alt=""><br>
                <p style="font-size:12pt">National School of Business Management (NSBM) held its inauguration of the Degree programmes for the September batch of students for the year 2015 at BMICH main hall on 13th October 2015. Over 1600 participants including both students and their parents attended to this formal reception of the new students who embarked on their journey towards personal achievement and professional success on this day. Hon. Mahinda Samarasinghe-Minister of Skills Development & Vocational Training participated as the Chief Guest for this event. Mr. P Ranepura-Secretary to the Ministry of Skills Development & Vocational Training, Dr. Karunasena Kodituwakku-Chairman of NIBM/NSBM, Dr. E A Weerasinghe-DG, NIBM & CEO, NSBM and Dr. D M A Kulasooriya-Director of NIBM & Director-Academic Affairs of NSBM (Acting), Senior Lecturers and Consultants of NSBM & NIBM were among the other dignitaries who graced the occasion. The successful second student intake of the year 2015 of NSBM has recorded over 650 registrations-a clear manifestation of the recognition of the institute have earned over the years with the emphasis and insistence on higher education that keeps abreast of global trends and demands. The rich portfolio of Undergraduate Degree programmes in Business Management, Information Technology, Business Communication and Creative Studies offers the best choices for higher education for school leavers. NSBM serves as the Degree school of NIBM and accredited to offer its own programmes accredited by University Grants Commission and those that are awarded by its internationally ranked partner universities, namely University College of Dublin-Ireland, Plymouth University-UK and Limkokwing University-Malaysia. NSBM is geared up for its upcoming milestone-the avant-garde Green University Complex-located in Pitipana, Homagama that is scheduled to be inaugurated for operations in December 2016. With a student capacity of 30,000 and a span of 26 acres of land, this venture is bound to take the Sri Lankan Higher Education to new heights. The students who enrol in NSBM for the year 2015 will thus join it to reap the benefits of the first ever Green University experience in the country.</p>
            </div>
            <br><br><br>
        <div class="col-sm-12">
        <footer>
        <?php include 'header and footer/footer.php'?>
        </footer>
        </div>
    </body>
</html>