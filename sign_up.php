<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>
        
         <title> Sign Up </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        <div class="raw signup">
        <div class="col-sm-offset-2 col-sm-11">
            <br><br><br>
            <div class="col-sm-offset-3">
                <h2 style="margin-left=70px;">Sign Up</h2>
            </div>
            <form action="register.php" method="post" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="fulname">Full Name:</label>
                    <div class="col-sm-6">
                        <input type="fname" class="form-control" id="fname" placeholder="Enter your full name with initials" name="fname" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2" for="username">User Name:</label>
                    <div class="col-sm-6">
                        <input type="uname" class="form-control" id="uname" placeholder="Enter a user name u like to use" name="uname" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="email" placeholder="Enter your Email" name="email" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2" for="innumber">Index Number:</label>
                    <div class="col-sm-6">
                        <input type="innumber" class="form-control" id="innumber" placeholder="Enter your index number" name="innumber" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2" for="password">Password:</label>
                    <div class="col-sm-6">
                        <input type="pword" class="form-control" id="pword" placeholder="Enter a password" name="pword" required>
                    </div>
                </div>
                
                <div class="form-group">
                      <label class="control-label col-sm-2" for="faculty">Faculty:</label>
                      <div class="col-sm-6">
                          <select class="form-control" id="faculty" name="faculty" required>
                              <option disabled selected> Choose your Faculty </option>
                              <option>Computing</option>
                              <option>Management</option>
                              <option>Engineering</option>
                          </select>
                      </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Register</button>
                        <button type="reset" class="btn btn-default">Retry</button><br><br>
                    </div>
                </div>
            </form>
        </div>
        </div>
        <div class="col-sm-12">
        <footer>
        <?php include 'header and footer/footer.php'?>
        </footer>
        </div>
    </body>
</html>