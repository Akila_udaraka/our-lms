<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>

         <title> About Us </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw cola">
                <!-- Team Section -->
    <section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Our Team</h2>
                    <h3 class="section-subheading text-muted">National School Of Computing 15.1 Batch.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="team-member">
                        <img src="Photos/img12.jpg" class="img-responsive img-circle" alt="">
                        <h4>Akila Wijesinghe</h4>
                        <p class="text-muted">BSC-PLY-COM-116</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="https://twitter.com/akilauw46"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/akila.udaraka"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="https://www.linkedin.com/in/akila-wijesinghe"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-sm-3">
                    <div class="team-member">
                        <img src="Photos/img15.jpg" class="img-responsive img-circle" alt="">
                        <h4>Tharindu Udaya</h4>
                        <p class="text-muted">BSC-PLY-COM-048</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="https://twitter.com/tharindu_udaya"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/loranso.megar"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="https://www.linkedin.com/in/tharindu-udaya"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-sm-3">
                    <div class="team-member">
                        <img src="Photos/img13.jpg" class="img-responsive img-circle" alt="">
                        <h4>Kaushal Manamperi</h4>
                        <p class="text-muted">BSC-PLY-COM-059</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="https://twitter.com/manamperilk"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/profile.php?id=100009554526740&fref=ts"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="https://www.linkedin.com/in/kaushal-manamperi"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <img src="Photos/img14.jpg" class="img-responsive img-circle" alt="">
                        <h4>Hasura Mapa</h4>
                        <p class="text-muted">BSC-PLY-COM-062</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="https://twitter.com/hasuramapa"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/hcm1995?fref=ts"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
            </div>
        <div class="col-sm-12">
        <?php include 'header and footer/footer.php'?>
        </div>
    </body>
</html>