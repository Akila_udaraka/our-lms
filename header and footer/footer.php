<footer id="footerWrapper">
    <div class="row ftr">
        
        <div class="col-sm-2" style=" margin-left:50px;">
            <div class="social_container">
                <h3>Follow us </h3>

                  <div class="social-icons">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                  </div>

                  <div class="social-icons">
                    <a href="#"><i class="fa fa-twitter"></i></a>
                  </div>

                  <div class="social-icons">
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                  </div>

                  <div class="social-icons">
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                  </div>
            </div>
        </div>
        
        <div class="col-sm-3 aki">
            <div class="footer-center">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p><span>309, High Level Road</span> Colombo 05, Sri Lanka</p>
                </div>
<br>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>Tel: +94 (11) 544 5000<br>
                    Fax: +94 (11) 544 5009</p>
                </div>
<br>
                <div>
                    <i class="fa fa-envelope"></i>
                    <p><a href="mailto:support@company.com">Email: info@nsbm.lk</a></p>
                </div>

            </div>
        </div>
        
        <div class="col-sm-6">
            <div class="footerWidget">
                <img src="photos/logo.jpg" alt="footer logo" id="footerLogo" style="height:150px; width:150px;">
                <h4>About the system</h4>
                <p class="footer-company-about" style="margin-left:30px; margin-right:30px; margin-bottom:50px;">
                    
                    The vision of this website is to provide pedagogical quality, excellence & innovation e-learning facility to all students.So the main purpose of this website is to use technology in learning and teaching situations via offering self-learning pattern by employing multi-media to allow the learner be the center of the educational process and to create academic creative environment to achieve quality learning.

                </p>
            </div>
        </div>
</div>

    <section id="footerRights">
        <div class="row kau">
            <div class="col-md-12">
                <p style="margin-left: 28px; margin-top:10px;">Copyright © 2016  / All rights reserved.</p>
            </div>
        </div>
    </section>
</footer>