<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>
        
         <title> Log In </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw login">
            <div class="col-sm-12">
                <h2 class="log1">Log in to get more</h2><br>
            </div>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="username">User Name:</label>
                   <div class="col-sm-4">
                     <input type="username" class="form-control" id="username" placeholder="User Name:">
                   </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4" for="pwd">Password:</label>
                   <div class="col-sm-4">
                     <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                   </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-6" id="cbox">
                    <div class="checkbox">
                      <label><input type="checkbox"> Remember me</label>
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-6">
                    <button type="submit" class="btn btn-default">Log In</button>
                  </div>
                </div>
            </form>
        </div>
        
        <div>
            <div class="col-sm-10 col-sm-offset-5">
                <h4>New to Minning:</h4>
                <a href="sign_up.php" class="btn btn-info" role="button">Sign Up</a><br><br>
            </div>
        </div>
        
        <div class="col-sm-12">
        <footer>
        <?php include 'header and footer/footer.php'?>
        </footer>
        </div>
    </body>
</html>