<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>
         <title> Learing Management Sysyem </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw">
            <img src="Photos/mgt3.jpg" class="img-responsive" alt="">
        </div>
        
        <div class="col-sm-10 col-sm-offset-1 row">
            <h2 class="row1">"The roots of the Education are bitter,<br>but the fruit is sweet!</h2>
        </div>
        
        <div class="raw col2">
            <div class="col-sm-4 col99">
                <a href="it.php"><img src="Photos/computing.jpg" class="img-responsive" alt="">
                    <h3 class="heading"><b>Faculty Of Computing</b></h3></a>
                <p class="p2">The School of Computing is another main school at NSBM which provides world class training and education in Computing, Information Technology, Design, Mathematics and Statistics at undergraduate as well as post graduate levels. The School of Computing too partners up with the world top ranking universities, University College Dublin, Ireland, Limkokwing University of Creative Arts & Technology, Malaysia and University of Plymouth, UK to provide undergraduates with highly recognized International Degrees
                </p>
            </div>
            
            <div class="col-sm-4 col98">
                <a href="mgt.php"><img src="Photos/mgt.jpg" class="img-responsive" alt="">
                    <h3 class="heading"><b>Faculty Of Management</b></h3></a>
                <p class="p2">The School of Business of NSBM is the ideal institute for any undergraduate interested in pursuing a career in the Business field. The School nurtures students with a business mind and moulds them into fully fledged business leaders of the future. The drive behind achieving this goal is the passion for excellence and perfection that surrounds the School of Business in its methods of teaching, learning, research and networking with the business community.
                </p>
            </div>
            
            <div class="col-sm-4 col97">
                <a href="eng.php"><img src="Photos/engineering.jpg" class="img-responsive" alt="">
                    <h3 class="heading"><b>Faculty Of Engineering</b></h3></a>
                <p class="p2">The School of Engineering is the newest addition to the faculties at NSBM. The School of Engineering offers degrees in Computer Engineering and Electronics in collaboration with the internationally acclaimed universities.NSBM's School of Engineering prides itself for keeping up to date with the advancing technologies through research and development activities, staff training and networking with international communities.
                </p>
            </div>
        </div>
        
        <div class="raw col3">
            <div class="col-sm-5 col-sm-offset-1 col96">
                <a href="news.php"><h3 class="event"> <b>Nsbm We Got Tallent</b> </h3>
                    <img src="Photos/img001.jpg" class="img-responsive" alt=""></a>
                <p class="ev">NSBM We got Talent, the massive talent search organised by the Management of National School of Business Management (NSBM) for the third consecutive year was held in Nelum Pokuna Mahinda Rajapaksha Theatre on 18 December 2015.</p>
            </div>
            
            <div class="col-sm-5 col-sm-offset-1 col95">
                <a href="news.php"><h3 class="event"> <b>American Degrees soon at NSBM</b> </h3>
                <img src="Photos/img002.jpg" class="img-responsive" alt=""></a>
                <p class="ev">NSBM the Nation's Premier Degree School will launch degree programmes affiliated to SRU, State University of Pennsylvania, United States very soon.A letter of intent on this regard was signed and exchanged between Dr. E A Weerasinghe-Vice Chancellor of NSBM and Dr. Cheryl J. Norton President of SRU on 8th of January 2016. This will bring the opportunity to Sri Lankan students to follow world-class American degree programmes in the fields of Bio-Science, Chemistry, Physics, Computing and Management at NSBM Green University.</p>
            </div>
        </div>
        
        <div class="col-sm-12 raw2">
            <div class="raw3">
                <a href="log_in.php" class="btn btn-login lora"><span class="glyphicon glyphicon-user" style="margin-left:10px; margin-right:20px; margin-top:10px;"></span>LOGIN</a>
                <a href="sign_up.php" class="btn btn-signup lora"><span class="glyphicon glyphicon-user" style="margin-left:10px; margin-right:20px; margin-top:10px;"></span>SIGN UP</a>
            </div>
        </div>
        
        <div class="col-sm-12 raw4">
        <?php include 'header and footer/footer.php'?>
        </div>
    </body>
</html>