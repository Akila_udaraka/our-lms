<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>

         <title> Faculty of Engineering </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw nw1">
            <div class="col-sm-12">
                <h1 class="ne1">Faculty of Engineering</h1>
            </div>
        </div>
        
        <div class="raw nw2">
            <div class="col-sm-10 col-sm-offset-1">
                <img src="Photos/eng11.jpg" class="img-responsive">
                <p>
                    The School of Engineering is the newest addition to the faculties at NSBM. The School of Engineering offers degrees in Computer Engineering and Electronics in collaboration with the internationally acclaimed universities.

                    NSBM's School of Engineering prides itself for keeping up to date with the advancing technologies through research and development activities, staff training and networking with international communities. Further, the school hopes to acquire, promote, develop and disseminate knowledge of engineering sciences in order to improve the quality of life by equipping future generations with the right attitudes and skills to grow into competent individuals who could be considered an asset to the nation
                    
<h3>Entry requirements</h3>

A Levels
<ul>
    <li>Grades=AAA-AAB</li>
    <li>Subjects-Mathematics and Physics at grade A, or Mathematics and Further Mathematics at grade A and Physics at grade B.</li>
    <li>AS LevelsFor UK-based students a pass in a further subject at AS level or equivalent is required.</li>
    <li>GCSEs-English Language and Mathematics at grade C. </li>
    <li>For UK-based students, a grade C or equivalent in a foreign language (other than Ancient Greek, Biblical Hebrew or Latin) is required. UCL provides opportunities to meet the foreign language requirement following enrolment, further details at: www.ucl.ac.uk/ug-reqs</li>
       </ul>
IB Diploma
<ul>
    <li>Points-36-38</li>
    <li>Subjects-A score of 17-18 points in three higher level subjects including Mathematics and Physics at grade 6, with no score below 5.</li>
       </ul>
UK applicants qualifications-

For entry requirements with other UK qualifications accepted by UCL, choose your qualification from the list below:


International applications-

In addition to A level and International Baccalaureate, UCL considers a wide range of international qualifications for entry to its undergraduate degree programmes.


       <h3>Undergraduate Preparatory Certificates</h3>

UCL offers intensive one-year foundation courses to prepare international students for a variety of degree programmes at UCL.

The Undergraduate Preparatory Certificates (UPCs) are for international students of high academic potential who are aiming to gain access to undergraduate degree programmes at UCL and other top UK universities.

For more information see our website: UCL Undergraduate Preparatory Certificate.

       <h3>English language requirements</h3>

If your education has not been conducted in the English language, you will be expected to demonstrate evidence of an adequate level of English proficiency. Information about the evidence required, acceptable qualifications and test providers can be found on our English language requirements page.

The English language level for this programme is: Standard

A variety of English language programmes are offered at the UCL Centre for Languages & International Education.
       <h3>Degree benefits</h3>
<ul>
    <li>Our top-quality laboratory and testing facilities include, amongst others, materials testing equipment, wind tunnels, two large wave tanks and an array of engine test cells.</li>
    <li>You will benefit from our internationally renowned research expertise as this cutting-edge knowledge is passed on to you through our teaching.</li>
    <li>The flexible programme structure enables you to transfer between the BEng and MEng degree programmes up to the end of the second year.</li>
    <li>We offer you a degree that is highly respected both within the UK and abroad. The programme is accredited by the Institution of Mechanical Engineers (IMechE).</li></ul>
    <h3>Degree structure</h3>

In each year of your degree you will take a number of individual courses, normally valued at 0.5 or 1.0 credits, adding up to a total of 4.0 credits for the year. Courses are assessed in the academic year in which they are taken. The balance of compulsory and optional courses varies from programme to programme and year to year. A 1.0 credit is considered equivalent to 15 credits in the European Credit Transfer System (ECTS).
<br>
The BEng programme is similar to the MEng programme for the first two years and you may transfer between them at the end of the second year, depending on certain criteria. Applying for a MEng initially helps keep your options open. The BEng is suitable for students who might wish to undertake graduate studies in the future (e.g. an MSc or PhD) or who do not necessarily seek Chartered Engineer status after they graduate.
<br><br>
The programme offers you a wide choice of optional courses in the final year. Courses are grouped under the following subject areas: Applied mechanics and materials; Fluid mechanics and thermodynamics, and Management and production engineering. Other optional courses are also available to you, including a modern language.
<br>
You will undertake an individual project as a major component of the third year.
<br><br>
This degree is part of the Integrated Engineering Programme (IEP), a teaching framework that engages students in specialist and interdisciplinary activities designed to create well-rounded graduates with a strong grasp of the fundamentals of their discipline and a broad understanding of the complexity and context of engineering problems. Students register for a core discipline, but also engage in activities that span departments so the development of fundamental technical knowledge takes place alongside specialist and interdisciplinary research-based projects and professional skills. This creates degrees encouraging professional development, with an emphasis on design and challenging students to apply knowledge to complex problems.

                </p></div></div>
        <div class="col-sm-12">
            <footer>
                <?php include 'header and footer/footer.php'?>
            </footer>
        </div>
        
    </body>
</html>