<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>

         <title> Faculty of Management </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw nw1">
            <div class="col-sm-12">
                <h1 class="ne1">Faculty of Management</h1>
            </div>
        </div>
        
        <div class="raw nw2">
            <div class="col-sm-10 col-sm-offset-1">
                <img src="Photos/mgt3.jpg" class="img-responsive">
                <p>
                    The School of Business of NSBM is the ideal institute for any undergraduate interested in pursuing a career in the Business field. The School nurtures students with a business mind and moulds them into fully fledged business leaders of the future. The drive behind achieving this goal is the passion for excellence and perfection that surrounds the School of Business in its methods of teaching, learning, research and networking with the business community.<br><br>

                    The School of Business offers many degree programs: Business Management, Human Resource Management, Law and Accounting in collaboration with world renowned universities, University College Dublin, Ireland, Limkokwing University Malaysia and University of Plymouth, UK. This gives our graduates the opportunity to gain an International degree while living and working in Sri Lanka.<br><br>

                    Students will find learning at NSBM's School of Business quite a unique and interesting experience as undergraduates are also given the taste of real life business experiences while learning the theories behind it at class. Through this process, the school strives to prepare business undergraduates to face any challenges in the real business world as they will be equipped with excellent problem solving and analytical capabilities. The School of Business takes special care to ensure that all students are provided with intellectual depth and abundant resources as well as individual attention.<br><br>

                    Each department of the School of Business is dedicated to its students and in providing them the best possible educational experience. So, if you're looking for a bright future in Business, NSBM School of Business is the place to be!

    <h2> Management Information Systems (MIS)</h2>

Related Terms: Automation

A management information system (MIS) is a computerized database of financial information organized and programmed in such a way that it produces regular reports on operations for every level of management in a company. It is usually also possible to obtain special reports from the system easily. The main purpose of the MIS is to give managers feedback about their own performance; top management can monitor the company as a whole. Information displayed by the MIS typically shows "actual" data over against "planned" results and results from a year before; thus it measures progress against goals. The MIS receives data from company units and functions. Some of the data are collected automatically from computer-linked check-out counters; others are keyed in at periodic intervals. Routine reports are preprogrammed and run at intervals or on demand while others are obtained using built-in query languages; display functions built into the system are used by managers to check on status at desk-side computers connected to the MIS by networks. Many sophisticated systems also monitor and display the performance of the company's stock.
       
           
           
    <h3>ORIGINS AND EVOLUTION</h3>
The MIS represents the electronic automation of several different kinds of counting, tallying, record-keeping, and accounting techniques of which the by far oldest, of course, was the ledger on which the business owner kept track of his or her business. Automation emerged in the 1880s in the form of tabulating cards which could be sorted and counted. These were the punch-cards still remembered by many: they captured elements of information keyed in on punch-card machines; the cards were then processed by other machines some of which could print out results of tallies. Each card was the equivalent of what today would be called a database record, with different areas on the card treated as fields. World-famous IBM had its start in 1911; it was then called Computing-Tabulating-Recording Company. Before IBM there was C-T-R. Punch cards were used to keep time records and to record weights at scales. The U.S. Census used such cards to record and to manipulate its data as well. When the first computers emerged after World War II punch-card systems were used both as their front end (feeding them data and programs) and as their output (computers cut cards and other machines printed from these). Card systems did not entirely disappear until the 1970s. They were ultimately replaced by magnetic storage media (tape and disks). Computers using such storage media speeded up tallying; the computer introduced calculating functions. MIS developed as the most crucial accounting functions became computerized.


    <h3>MIS AND SMALL BUSINESS</h3>
If MIS is defined as a computer-based coherent arrangement of information aiding the management function, a small business running even a single computer appropriately equipped and connected is operating a management information system. The term used to be restricted to large systems running on mainframes, but that dated concept is no longer meaningful. A medical practice with a single doctor running software for billing customers, scheduling appointments, connected by the Internet to a network of insurance companies, cross-linked to accounting software capable of cutting checks is de facto an MIS. In the same vein a small manufacturer's rep organization with three principals on the road and an administrative manager at the home office has an MIS system, that system becomes the link between all the parts. It can link to the inventory systems, handle accounting, and serves as the base of communications with each rep, each one carrying a laptop. Virtually all small businesses engaged in consulting, marketing, sales, research, communications, and other service industries have large computer networks on which they deploy substantial databases. MIS has come of age and has become an integral part of small business.


       <h3>Entry requirements</h3>



       <h3>A Levels</h3>
<ul>
    <li>Grades   -   AAA</li>
    <li>Subjects   -Mathematics required.</li>
    <li>AS Levels-For UK-based students a pass in a further subject at AS level or equivalent is required.</li>
    <li>GCSEs-English Language and Mathematics at grade C. For UK-based students, a grade C or equivalent in a foreign language (other than Ancient Greek, Biblical Hebrew or Latin) is required. UCL provides opportunities to meet the foreign language requirement following enrolment, further details at: www.ucl.ac.uk/ug-reqs
    </li>
       </ul>
       <h3>IB Diploma</h3>
<ul>
    <li>
        Points - 38</li>
    <li>Subjects - A score of 18 points in three higher level subjects, including grade 6 in Mathematics, with no score lower than 5.</li>
       </ul>

    <h3>UK applicants qualifications</h3>

For entry requirements with other UK qualifications accepted by UCL, choose your qualification from the list below:
                </p></div></div><br><br>
            
        <div class="col-sm-12">
            <footer>
                <?php include 'header and footer/footer.php'?>
            </footer>
        </div>
        
    </body>
</html>