<!DOCKTYPE HTML>
<html>
    <head>
        <?php include 'header and footer/head.php'?>

         <title> Faculty of Computing </title>
    </head>
    <body style="overflow-x: hidden">
            <?php include 'header and footer/header.php'?>
        
        <div class="raw nw1">
            <div class="col-sm-12">
                <h1 class="ne1">Faculty of Computing</h1>
            </div>
        </div>
        
        <div class="raw nw2">
            <div class="col-sm-10 col-sm-offset-1">
                <img src="Photos/comfac.jpg" class="img-responsive" alt=""><br>
                <p>
                    The School of Computing is another main school at NSBM which provides world class training and education in Computing, Information Technology, Design, Mathematics and Statistics at undergraduate as well as post graduate levels. The School of Computing too partners up with the world top ranking universities, University College Dublin, Ireland, Limkokwing University of Creative Arts & Technology, Malaysia and University of Plymouth, UK to provide undergraduates with highly recognized International Degrees. The innovative teaching methods along with the latest state of the art equipment form the perfect blend that motivates our students to do their best and reach their goals with ease.<br><br>

                    The school provides top notch research, training and development services that will help students acquire new knowledge along with the best practices in their respective disciplines. The School of Computing aims to be among the foremost centre of excellence in Research and Development (R&D) and advance education in Computing while taking into consideration National as well as Regional requirements for Information and Communication Technology. Further, The School of Computing places equal emphasis on both theory and practice of all aspects of the Computing field. Therefore, our students will have sufficient hands on experience to take up any working assignment in their respective IT fields at the end of their degree programmes.
                </p>
                <h3>Entry Requirements</h3>
                
A Levels
<ul>
    <li>Grades-A*AA</li>
    <li>Subjects-Mathematics required.</li>
    <li>AS Levels-For UK-based students a pass in a further subject at AS level or equivalent is required.</li>
    <li>GCSEs-English Language and Mathematics at grade C. </li>
    <li>For UK-based students, a grade C or equivalent in a foreign language (other than Ancient Greek, Biblical Hebrew or Latin) is required. UCL provides opportunities to meet the foreign language requirement following enrolment, further details at: www.ucl.ac.uk/ug-reqs</li>
       </ul>
       
IB Diploma
<ul>
    <li>
        Points-39</li>
    <li>Subjects-A total of 19 points in three higher level subjects including Mathematics, with no score below 5.</li>
       </ul>
UK applicants qualifications-

For entry requirements with other UK qualifications accepted by UCL, choose your qualification from the list below:


International applications-

In addition to A level and International Baccalaureate, UCL considers a wide range of international qualifications for entry to its undergraduate degree programmes.


       <h3>Undergraduate Preparatory Certificates</h3>

UCL offers intensive one-year foundation courses to prepare international students for a variety of degree programmes at UCL.

The Undergraduate Preparatory Certificates (UPCs) are for international students of high academic potential who are aiming to gain access to undergraduate degree programmes at UCL and other top UK universities.

For more information see our website: UCL Undergraduate Preparatory Certificate.

       <h3>English language requirements</h3>

If your education has not been conducted in the English language, you will be expected to demonstrate evidence of an adequate level of English proficiency. Information about the evidence required, acceptable qualifications and test providers can be found on our English language requirements page.

The English language level for this programme is: Standard

A variety of English language programmes are offered at the UCL Centre for Languages & International Education.
       
       <h3>Degree benefits</h3>
<ul>
    <li>World-class researchers have designed and teach our courses to ensure that our material is cutting-edge.</li>
    <li>Located in purpose-built accommodation, the department offers excellent laboratory and experiment facilities in a friendly and personal learning environment.</li>
    <li>Our location in the centre of London strengthens our close associations with industry and the financial sector, and offers you extensive opportunities for developing contacts with potential employers.</li>
    <li>The degree is part of an integrated programme across engineering providing opportunities to broaden your horizons through interactions with other disciplines.</li>
       </ul>
    <h3>Degree structure</h3>

In each year of your degree you will take a number of individual courses, normally valued at 0.5 or 1.0 credits, adding up to a total of 4.0 credits for the year. Courses are assessed in the academic year in which they are taken. The balance of compulsory and optional courses varies from programme to programme and year to year. A 1.0 credit is considered equivalent to 15 credits in the European Credit Transfer System (ECTS).
<br><br>
The Computer Science MEng and BSc programmes share a common core for the first two years. The aim of this core is to cover the essential material required by all computer scientists, whatever their particular interest or specialisation. The core covers all the main strands of computer science: architecture, programming, theory, design and mathematics.
<br><br>

In your final year you will carry out an individual supervised project, and follow core courses in operating systems, computational complexity, and technology management and professional issues. In addition you take courses selected from within the department.
<br>
This degree is part of the Integrated Engineering Programme (IEP), a teaching framework that engages students in specialist and interdisciplinary activities designed to create well-rounded graduates with a strong grasp of the fundamentals of their discipline and a broad understanding of the complexity and context of computer science problems. Students engage in activities that span departments so the development of fundamental technical knowledge takes place alongside specialist and interdisciplinary research-based projects and professional skills. This creates degrees encouraging professional development, with an emphasis on design and challenging students to apply knowledge to complex problems.
            </div></div><br><br>
        <div class="col-sm-12">
            <footer>
                <?php include 'header and footer/footer.php'?>
            </footer>
        </div>
        
    </body>
</html>